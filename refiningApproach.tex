%!TEX root = /Volumes/Storage/Dropbox/AU Uni/Speciale/LaTex/AnomalyDetection.tex

\section{Refining and Evaluating the Approach}

\textit{This chapter will look at the system as a whole, present results and discuss implication of various parameters that influence performance of the system.} \\

To get a visual impression on performance of the system, figure \ref{fig:knnDistVisualizedOnImage} displays the detected anomaly with the distance measure determined by KNN. This image has been selected as it represents an average image in the sequence.

\begin{figure}[H]
	\centerline{\includegraphics[width=0.7\linewidth]{images/classifiers/knnBoundingBox.pdf}}
	\caption{K-NN shown on top of annotated input image. The shown numerical value is the distance to the 10'th nearest neighbor. This value is computed for each of the 169 values in the feature map(13x13). The K-NN is fedd with the top four features determined.} 
	\label{fig:knnDistVisualizedOnImage} 
\end{figure}

It is clearly seen, that the annotated anomaly in the scene, gets far greater distance measures than the surroundings, which is almost zero everywhere. For evaluation, values are compared pixel-by-pixel in the 13x13 feature maps produced. The annotation is expanded to include all feature map pixels it touches, resulting in the bounding box being expanded to the nearest gridline. This precision can be improved by modifying the CNN to include more pixels, if it is desired in a production system.  

Looking at the system as a whole, its performance can be presented as the percentage of True Positives and False Positives implied by a chosen K-NN Threshold. Varying the threshold for the K-NN distance to assign a given feature vector as either normal or abnormal, leads to the amount of True Positives and False Positives presented in table \ref{table:varyingThreshold}. 
It shows the amount of true positives vs. the amount of false positives as the threshold is altered. So in order to correctly detect 80\% of all anomalies, a rate of 14\% false alarms has to be accepted.

\setlength{\tabcolsep}{6pt}
\renewcommand{\arraystretch}{1}
\begin{table}[H]
\centering
\begin{tabular}{| c | r | r |}
  \hline
  Threshold & True Positives & False Positives \\ \hline
  218.52 & 0 & 0 \\ \hline
  13.96 & 0.75 & 0.09 \\ \hline % index: 177750
  8.36 & 0.80 & 0.14 \\ \hline % index: 213500
  3.38 & 0.85 & 0.25 \\ \hline % index: 273000
  0.19 & 0.90 & 0.60 \\ \hline % index: 427500
  0 & 0.93 & 0.85 \\
  \hline
\end{tabular}
\caption{Varying threshold and its implication on True Positives and False Positives of the system.}
\label{table:varyingThreshold}
\end{table}

These values are calculated by running the entire system on a test part of the SAFE dataset containing 400 annotated anomalies in 300 individual images. Following the approach depicted in chapter \ref{chap:approach}, the procedure is as following:

\begin{enumerate*}
	\item Feed image to the CNN, which generates 256 feature maps. 
	\item Calculate The the K-NN distance to the 10'th nearest neighbor, in the model for normality. This is done for every entry in the 13x13 feature map.
	\item Threshold every distance value, and assess whether detection conforms with bounding box or not.
\end{enumerate*}

These values are also presented in a ROC curve in figure \ref{fig:rocCurveSystem}. ROC (Receiver Operating Characteristics) is a way of assessing performance of binary classification systems. It shows clearly the tradeoff between true positives and false positives when varying the threshold. The area under the curve is a measure of system accuracy \cite{rocPlotting}.

\begin{figure}[H]
    \centerline{\includegraphics[width=0.7\linewidth]{images/classifiers/roc_curve55feat.pdf}}
    \caption{K-NN ROC curve based on evaluation on the test part of the dataset, containing 400 annotations in 300 images. 50 of the best features are included in the classification.} % Using matlabs plotroc() implementation.
	\label{fig:rocCurveSystem} 
\end{figure}

Generally this shows very good distinction capability between normal and anomalies in the scene. To visualize the diffent cases of false positives, false negatives and so forth, appendix \ref{appendix:syseval} presents several images with both annotations and detections marked. Here it is clearly seen that false positive detections by the systems are of other nature than the normal in the scene, such as houses in the horizon. Also, the extend of objects yields false positives due to the pixel-wise evaluation. Missed detections, false negatives, are often small or occluded objects.\\

A natural step is now to investigate which parameters that influences the performance of the system. An analysis of the system as a whole, led to the following main parameters that influence performance of the entire system:

\begin{description}
	\item[$n$] \indent How many neighbors to assess for K-NN distance.
	\item[$T$] - Threshold for distance, d, to N'th neighbor. Already assessed.
	\item[$d_{measure}$] - Which methodology to use for distance measure: euclidian, mahalanobis etc.
	\item[$n_f$] - How many features to include. Chapter \ref{subsec:featureAnalysisAbn} identified good candidates.
	\item[$s$] - Sensitivity to bounding box overlap
\end{description}

These parameters can be investigated independently. As the scope of this project is investigation of features, it is naturally to assess how many features to include. A way to do this, is to calculate the AUC (Area Under Curve) of the ROC curve, as this directly correlates with the accuracy of the system. Therefore AUC as a function of amount of features included is plotted in figure \ref{fig:AUCrocCurveSystem}:

\begin{figure}[H]
    \centerline{\includegraphics[width=\textwidth]{images/classifiers/allAUC250.pdf}}
  	\caption{AUC of ROC curve for a varying number of features included.}
	\label{fig:AUCrocCurveSystem} 
\end{figure}

This clearly shows that including more features improves the robustness of the system. The performance shows stagnation around 88\%, where a tradeoff can be made on the number of features to include, as this has a great impact on the computational requirements for the system.


Figure \ref{fig:AUCrocCurveSystem} further indicates that the ordering of good features for anomaly detection, identified in chapter \ref{subsec:featureAnalysisAbn} is correct. That is the performance improves, but less and less for each feature included. If a poor feature would be selected at fist, a low increase would be seen, and then a jump for a good feature. Other feature evaluation techniques exists, such as forward and backward sequential feature selection \cite{PatternRecognitionBook}, but is not investigated further as the feature ranking approach by analysis proved correct sorting of features. 

The selected treshold is 8.36, that implies correctly detection 80\% of annotated anomalies. This is the distance to the 10'th nearest neighbor.

\subsection{Discussion of Results}
% Hvorfor ser resultaterne ud som de gør
The results presented are dependent on numerous parameters and decisions as presented in the listing on the previous page. 

Precision of the detected object and overlap with the manually labeled bounding box will have a great impact directly on the results, and the evaluation approach taken. The evaluation methodology used, with comparing each pixel in the 13x13 feature map, is suiting when assessing the performance with regards to the CNN features and their precision. For a production system, it might be satisfactory just to know that the anomaly is present and its approximate location in the scene. Evaluation for the ImageNet challenge, localization requirements are merely 50\% bounding box overlap \cite{imageNet}. Using this evaluation methodology would most likely yield significantly less false positives as the requirement for extend of objects would be loosened. The amount of false negatives would remain more or less the same, as these are mostly due to small or occluded objects.

% Hvordan kan resultaterne bruges
The hypothesis presented, that the pre-trained deep learning features would suppress the noise in grass texture and yield distinctive values when anomalies present in the scene, has been verified. Furthermore a system is capable of discerning between normal and anomalies based on these features, with promising results.

%Whether 80\% correct classification is satisfactory for a real life system is doubtfull at best, but t

These results clearly shows that deep learning features detects objects that stand out in the scene. 

\newpage








