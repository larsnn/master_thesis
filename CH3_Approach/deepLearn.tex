%!TEX root = /Volumes/Storage/Dropbox/AU Uni/Speciale/LaTex/AnomalyDetection.tex

\subsection{Deep Learning and CNN}
\textit{This section is devoted to firstly motivate for the use of CNN and explain the chosen convolutional neural network setup. Secondly to unfold the concept of deep learning and the components of a CNN. } \\

CNN and deep learning techniques have achieved great popularity recently for various computer vision tasks. Most notably the field of image classification has seen great improvement by using this technique \cite{YannLeCun}. It is therefore, interesting to use a CNN for the task at hand. The idea is to use the CNN solely for feature extraction. Thereby normal classification flow can be achieved. The traditional approach is to use a fully connected layer and softmax to perform the classification step. The classification in this case is not conforming with the traditional labelling approach, as the intent is to do a semi-supervised learning step where only the model for normality is used. 

The approach visualized below in figure \ref{fig:deeplearn}, as a modification of the conventional deep learning approach shown on top in figure \ref{fig:deeplearn}.

\begin{figure}[H]
	\begin{subfigure}{\textwidth}
		\centerline{\includegraphics[width=\linewidth]{images/DeepLearningBasics.pdf}}
	\end{subfigure}

	\vspace{2.00mm}
	\unskip\ \hrule height 0.1ex\ 
	\vspace{2.00mm}

	\begin{subfigure}{\textwidth}
		\centerline{\includegraphics[width=\linewidth]{images/DeepLearningBasicsApproach.pdf}}
	\end{subfigure}

	\caption{Deep learning CNN basics, above is the conventional approach as presented by \cite{VisualizingCNN}. Below the proposed approach for this thesis, to use the resulting feature maps as feature vector input to a trained Anomaly Detection module.}
	\label{fig:deeplearn}
	
\end{figure}

\subsubsection*{The concept of deep learning}
Deep machine learning, covers a branch of machine learning that attempts to model high-level abstractions in data by using multiple layers containing non-linearities. The trick is to learn the representation that best facilitates the given task, e.g. distinguishing between object classes in the ImageNet challenge.

A clear expense by the use of deep learning is the convolutional training phase required. Furthermore a large dataset is needed in order to capture a decent amount of variation for the application. Fortunately there is pre-trained models available, provided by frameworks such as Caffe\cite{Caffe}. Caffe will be used for the deep learning part of this project, as the Computer Vision Department at Aarhus University has experience in using it. Several pre trained CNN models are available for Caffe, avoiding the time-consuming learning step. The specific model used for this project is AlexNet\cite{DCNNdescriotionImageNet}. It shows superior performance on the ImageNet 2012 dataset, and is chosen as it is trained for recognition of humans and other objects.

\subsubsection*{Origitination of CNN}
Convolutional neural networks originated from neural networks (NN), and has won great popularity for its ability to train a relatively deep networks much faster than regular fully connected neural networks. Regular NNs would in theory perform better, but some authors describe, that the theoretically best performance is likely to be only slighly worse for CNNs \cite{DCNNdescriotionImageNet}.

To visualize the difference, figure \ref{fig:neuralNet} and figure \ref{fig:CnnStep} shows a neural network and a convolutional step respectively. It is seen that the neural network require training of multiple weight parameters pr. receptor in the input layer. Every spatial position is connected to every neuron and has a weight, that needs to be learned. In CNN, a neuron is only locally connected and shares weights, reducing the amount of weight parameters drastically. %A convelutional step is shown on figure \ref{fig:CnnStep},

\begin{figure}[H]
\centering
\begin{subfigure}{.45\textwidth}
  \centering
  \includegraphics[width=\linewidth]{images/neuralNet.png}
  \caption{Neural Net \cite[chapter 6]{DeepLearnExplained}}
  \label{fig:neuralNet}
\end{subfigure}%
\begin{subfigure}{.45\textwidth}
  \centering
  \includegraphics[width=\linewidth]{images/DeepLearnNeuron.png}
  \caption{Convolutional step in CNN \cite[chapter 6]{DeepLearnExplained}}
  \label{fig:CnnStep}
\end{subfigure}
\caption{}
\end{figure}


\subsubsection*{CNN Components} 
The components that make up a deep CNN are basically convolutions with a given filter at each layer, providing its output as input for the next layer. For AlexNet, and most others, additional steps are taken in between layers. These components are described in the following: \newline

\textbf{Convolution}
is in this image context merely multiplying with a kernel and summing the entries, as shown by this simple example: 

\vspace{10pt}

 $\begin{bmatrix}
  a & b & c \\
  d & e & f \\
  g & h & i
 \end{bmatrix} * \begin{bmatrix}
  1 & 2 & 3 \\
  4 & 5 & 6 \\
  7 & 8 & 9
 \end{bmatrix} = (1 \cdot i) + (2 \cdot h) + (3 \cdot g) + (4 \cdot f) + ... + (8 \cdot b) + (9 \cdot a) $

\vspace{10pt}

This loosely relates to a form of mathematical convolution \cite{WikiConv}.

\vspace{10pt}

\begin{wrapfigure}{r}{0.5\textwidth}
 \vspace{-25pt}
%  \begin{centering}
    \includegraphics[width=0.48\textwidth]{images/activation_funcs1.png}
%  \end{centering}
  \vspace{-10pt}
  \caption{Activation functions \cite{ActivationFunctions}}
  \vspace{25pt}
  \label{fig:activationFunctions}
\end{wrapfigure}

\textbf{Activation Functions} is used to model a neuron's output. Different options exists, shown in figure \ref{fig:activationFunctions}. ReLU (Rectified Linear Unit) is the preferred option for CNN as model converges, and thereby trains, several times faster than their traditional $tanh(x)$ counterparts \cite{DCNNdescriotionImageNet}. ReLUs are defined by:

\begin{equation}
	relu(x) = max(x,0)
\end{equation}
And applied between each convolutional and fully connected layer. \\% on the max pooled value. \\

\textbf{Max pooling} is a form of non-linear down sampling. Its purpose is to reduce the computation for the next layers. It does so by applying a grid on the input image and for each cell selecting the maximum value for input to the next layer. The size of the cells then determines the size of the output. Furthermore this step provides translational invariance. %A one-pixel move has eight possible outcomes, and with max pooling over a cell of 2x2, three of these eight possibles outcomes will produce the exact same result for the next layer. Thereby the invariance. \\% http://deeplearning.net/tutorial/lenet.html

\subsection*{Training the CNN parameters}
The deep learning part is mainly to determine the filters. The filters are basically a correlation or similarity measures when convolved with the input image, and we are thereby looking for patterns on the input image that is repeated frequently enough to be significant. This means that if we are presenting a lot of input images with trees, the Deep Learning algorithm would yield filters that represent trees as they are very frequent. 

As progressing through the layers, features get higer and higer level of abstraction. To visualize this, different input and their filters for layers 1-5 are illustrated on figure \ref{fig:deepLearnSlides}.

\begin{figure}[H]
\centering
\begin{subfigure}{\textwidth}
  \centering
  \includegraphics[width=\linewidth]{images/DeepLearnOxfordLayers1_1.pdf}
  \caption{}
\end{subfigure}

\begin{subfigure}{\textwidth}
  \centering
  \includegraphics[width=0.8\linewidth]{images/DeepLearnOxfordLayers2.png}
  \caption{}
\end{subfigure}
\caption{Deep learning convolutional network features visualized \cite{VisualizingCNN}. Shows that as you progress the layers, features get higher and higher level. At layer 4 its clearly to see that the features in the top-left corner represents a dog. }
\label{fig:deepLearnSlides}
\end{figure}

The technique for updating the filters is to use back propagation. It works by passing any error that might have been in the output, backwards through the layers. A loss function is then evaluated in order to minimize the error, updating the filter in the direction of the gradient. This optimization procedure is very computational expensive, as it iterates over a large range of possible filters and their combinations. In the Caffe implementation, even with GPU support, this can take days or weeks for a dataset of the size of ImageNet \cite{imageNet}.

A concern when optimizing the filters is overfitting \cite{DCNNdescriotionImageNet}. The concept is that the statistical model that is trained, is fitted too close to the dataset with its natural noise. The model then gets biased by this noise, away from the underlying relationship. AlexNet avoids this in the training by augmenting data and using a technique called dropout. Augmentation of data is just synthesizing data programmatically to add variance. Dropout consists of randomly disabling different neurons such that the dependent neurons cannot rely on presence on particular other neurons, forcing them to improve robustness.  % Improving neural networks by preventing co-adaptation of feature detectors..  